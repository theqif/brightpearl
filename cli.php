<?php

// Martin Robertson
// mansionhouseprojects@gmail.com
// 2013/06/10
// Assignment for BrightPearl, as defined in REQUIREMENT

require_once('Payroll.class.php');

if (! isset ($argv[1]))
{
    echo "In order to run the payroll report, a year must be provided\n";
    exit;
}

$year = intval($argv[1]);

$payroll = new Payroll ($year);
$payroll->report();

exit;

?>

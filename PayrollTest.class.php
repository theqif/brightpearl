<?php

// Martin Robertson
// mansionhouseprojects@gmail.com
// 2013/06/10
// Assignment for BrightPearl, as defined in REQUIREMENT

require_once('Payroll.class.php');

class PayrollTest extends PHPUnit_Framework_TestCase
{
    public function test2012 ()
    {
        $this->verifyYear(2012);
    }
    public function test2013 ()
    {
        $this->verifyYear(2013);
    }
    ///////////////////////////////////

    public function answers ($y)
    {
        $correct = Array
        (
            '2012' => (object) Array
            (
                '0'  => (object)Array('name'=>'January',    'base'=>31, 'bonus'=>18,),
                '1'  => (object)Array('name'=>'February',   'base'=>29, 'bonus'=>15,),
                '2'  => (object)Array('name'=>'March',      'base'=>30, 'bonus'=>15,),
                '3'  => (object)Array('name'=>'April',      'base'=>30, 'bonus'=>18,),
                '4'  => (object)Array('name'=>'May',        'base'=>31, 'bonus'=>15,),
                '5'  => (object)Array('name'=>'June',       'base'=>29, 'bonus'=>15,),
                '6'  => (object)Array('name'=>'July',       'base'=>31, 'bonus'=>18,),
                '7'  => (object)Array('name'=>'August',     'base'=>31, 'bonus'=>15,),
                '8'  => (object)Array('name'=>'September',  'base'=>28, 'bonus'=>19,),
                '9'  => (object)Array('name'=>'October',    'base'=>31, 'bonus'=>15,),
                '10' => (object)Array('name'=>'November',   'base'=>30, 'bonus'=>15,),
                '11' => (object)Array('name'=>'December',   'base'=>31, 'bonus'=>19,),
            ),
            '2013' => (object) Array
            (
                '0'  => (object)Array('name'=>'January',    'base'=>31, 'bonus'=>15,),
                '1'  => (object)Array('name'=>'February',   'base'=>28, 'bonus'=>15,),
                '2'  => (object)Array('name'=>'March',      'base'=>29, 'bonus'=>15,),
                '3'  => (object)Array('name'=>'April',      'base'=>30, 'bonus'=>15,),
                '4'  => (object)Array('name'=>'May',        'base'=>31, 'bonus'=>15,),
                '5'  => (object)Array('name'=>'June',       'base'=>28, 'bonus'=>19,),
                '6'  => (object)Array('name'=>'July',       'base'=>31, 'bonus'=>15,),
                '7'  => (object)Array('name'=>'August',     'base'=>30, 'bonus'=>15,),
                '8'  => (object)Array('name'=>'September',  'base'=>30, 'bonus'=>18,),
                '9'  => (object)Array('name'=>'October',    'base'=>31, 'bonus'=>15,),
                '10' => (object)Array('name'=>'November',   'base'=>29, 'bonus'=>15,),
                '11' => (object)Array('name'=>'December',   'base'=>31, 'bonus'=>18,),
            ),
        );

        return $correct[$y];
    }
    public function verifyYear($year)
    {
        $correct = $this->answers($year);
        $payroll = new Payroll($year);

        $this->assertEquals (12, count($payroll->months));

        foreach ($correct as $k => $m)
        {
            $this->assertEquals($m->name,  $payroll->months[$k]->month_name);
            $this->assertEquals($m->base,  $payroll->months[$k]->base);
            $this->assertEquals($m->bonus, $payroll->months[$k]->bonus);
        }
    }
}
?>

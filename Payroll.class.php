<?php

// Martin Robertson
// mansionhouseprojects@gmail.com
// 2013/06/10
// Assignment for BrightPearl, as defined in REQUIREMENT

date_default_timezone_set('UTC');

class Payroll
{
    public $year   = null;
    public $months = Array();

    function __construct ($year)
    {
        if (intval($year) < 1970)
        {
            echo "too early, sorry"; exit;
        }

        $this->year = $year;
        for ($m = 1; $m <= 12; $m++)
        {
            $this->months[] = $this->dates_for_month ($m);
        }
    }

//The output of the utility should be a CSV file,
//containing the payment dates for this year.
//The CSV file should contain a column for the month name,
//a column that contains the salary payment date for that month,
//and a column that contains the bonus payment date.
    function report()
    {
        $rows = Array("Month, Bonus, Salary");
        foreach ($this->months as $m)
            $rows[] = "$m->month_name, $m->bonus, $m->base";

        $this->save(implode("\n", $rows));
    }

// this should verify if the file already exists
// and the capacity to write should be verified
// should also be unit tested in PayrollTest
    function save ($content = '')
    {
        $res = fopen ("$this->year.csv", "w");
        fwrite ($res, $content);
        fclose ($res);
    }

    function dates_for_month ($m)
    {
        return (object)Array
        (
            'year'          => $this->year,
            'month'         => $m,
            'month_name'    => date("F",mktime(0,0,0,$m,1,$this->year)),
            'base'          => $this->get_base_date  ($m),
            'bonus'         => $this->get_bonus_date ($m), 
        );
    }

    //The base salaries are paid on the last day of the month unless that day is a Saturday or a Sunday (weekend) in which case they are paid on the Friday before.
    function get_base_date ($m)
    {
        $max = cal_days_in_month(CAL_GREGORIAN, $m, $this->year);
        // last 'non-weekend' day of month
        for ($d = $max; $d >= 26; $d--)
        {
            $dow = $this->dow ($d, $m);
            if ( ! $this->is_weekend ($dow) )
                return $d;
        }
        return 31;
    }
    //On the 15th of every month bonuses are paid for the previous month, unless that day is weekend. In that case, they are paid the first Wednesday after the 15th.
    function get_bonus_date ($m)
    {
        $dow = $this->dow(15, $m);

        if ( ! $this->is_weekend ($dow) )
            return 15;

        // 15th is saturday or sunday, so Wednesday is 18 or 19
        // Saturday :
        if ($dow == 6)
            return 19;

        // Sunday :
        return 18;
    }

    function is_weekend ($dow)
    {
        return (($dow == 0) || ($dow == 6)) ? true : false;
    }

    function dow ($d, $m)
    {
        return date ("w", $this->epoch($d, $m));
    }

    function epoch ($d, $m)
    {
        return mktime(0, 0, 0, $m, $d, $this->year);
    }

}

?>
